package particular.ejercicios;

import java.util.ArrayList;
import java.util.List;

public class Prueba {
	
	public static void main(String[] args) {
		// Obtener numero de pisos de la piramide a partir de la cantidad colectada y el precio de la lata indicada
		int numPisos = getPisos(1500, 2);
		System.out.println("Numero de pisos resultantes: " + numPisos);
		// Encontrar el numero de ocurrencias indicadas por el arreglo digitos en el arreglo lista
		int [] lista = {-18, -31, 81, -19, 111, -888};
		int [] digitos = {1, 8, 4};
		List<String> resultado = printOcurrences(lista, digitos);
		System.out.println(resultado);
		// Correccion de el metodo para pasar cadena a su espejo
		byteArray("abc");
		// Correccion del factorial
		int fac = factorial(3);
		System.out.println( "Factorial es: " + fac );
	}

	private static int factorial(int i) {
		int f = 0;
		if(i == 0) {
			return 1;
		}
		if(i == 1) {
			return 1;
		}
		
		if(i>0) {
			f = i * factorial(i-1);
			
		}
		return f;
	}
	
	private static void byteArray(String s) {
		//String [] abc = "abcdefghijklmnopqrstuvwxyz".split("");
		String abc = "abcdefghijklmnopqrstuvwxyz";
		String [] str = s.split("");
		
		//ArrayList<String> aabc = new ArrayList(abc)
		String nstr = "";
		
		for (String ss: str) {
			int idx = abc.indexOf(ss);
			if(idx >= 0)
				nstr += abc.charAt( (abc.length() - idx) - 1 );
		}
		
		System.out.println( nstr.toString() );
	}	
	
	private static int getPisos(int cantidad, int precio) {
		int numPisos = 0;
		int numLatas = cantidad / precio;
		int latasPorPiso = 0;
		int aux = 0;
		
		if(cantidad < precio) return 0;
		
		while((aux + latasPorPiso) < numLatas) {
			numPisos++;
			latasPorPiso = numPisos * numPisos;
			aux += latasPorPiso;
		}
		
		return numPisos;
	}

	
	private static List<String> printOcurrences(int [] lista, int [] digitos) {
		List<String> resultado = new ArrayList<String>();
		
		String a = "";
		for(int i: lista) {
			a += i;
		}
		
		String [] elementos = a.split("");
		
		for(int i = 0; i < digitos.length; i++) {
			String n = String.valueOf( digitos[i] );
			int con = 0;
			for(int j = 0; j < elementos.length; j++) {
				if( n.equals( elementos[j] ) ) {
					con++;
				}
			}
			resultado.add( digitos[i] + "," + con );
		}
		
		return resultado;
	}

}
